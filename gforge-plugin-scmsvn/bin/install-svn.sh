#! /bin/sh
# 
# $Id$
#
# Configure Subversion for Sourceforge
# Roland Mas, Gforge

set -e

if [ $(id -u) != 0 ] ; then
    echo "You must be root to run this, please enter passwd"
    exec su -c "$0 $1"
fi

gforge_chroot=$(grep ^gforge_chroot= /etc/gforge/gforge.conf | cut -d= -f2-)

case "$1" in
    configure)
	if [ ! -x /usr/local/share/gforge/commit-email.pl ]; then
		mkdir -p /usr/local/share/gforge
		cp /usr/share/subversion/hook-scripts/commit-email.pl \
		    /usr/local/share/gforge/commit-email.pl
		chown 0:0 /usr/local/share/gforge/commit-email.pl
		chmod 0755 /usr/local/share/gforge/commit-email.pl
	fi
        echo "Modifying inetd for Subversion server"
        # First, dedupe the commented lines
        update-inetd --remove  "svnserve stream tcp nowait.400 gforge_scm /usr/bin/svnserve svnserve -i -r $gforge_chroot" || true
        update-inetd --remove  "svn stream tcp nowait.400 scm-gforge /usr/bin/svnserve svnserve -i -r $gforge_chroot" || true
        update-inetd --add  "svn stream tcp nowait.400 scm-gforge /usr/bin/svnserve svnserve -i -r $gforge_chroot"
	/usr/lib/gforge/plugins/scmsvn/bin/install-viewcvs.sh
	;;

    purge)
        update-inetd --remove  "svnserve stream tcp nowait.400 gforge_scm /usr/bin/svnserve svnserve -i -r $gforge_chroot"
        update-inetd --remove  "svn stream tcp nowait.400 scm-gforge /usr/bin/svnserve svnserve -i -r $gforge_chroot"
	;;

    *)
	echo "Usage: $0 {configure|purge}"
	exit 1
esac
