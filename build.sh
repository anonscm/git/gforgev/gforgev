#!/bin/mksh
# $Id$

cd $(dirname $0)
for subdir in gforge gforge-plugin-scmsvn; do
	cd $subdir
	dpkg-buildpackage -us -uc -rfakeroot || exit 1
	debian/rules clean
	cd ..
done
exit 0
