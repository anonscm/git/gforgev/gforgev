<?php
/* RSSReader 0.2.1 - a parser hook for MediaWiki
 * Copyright (C) 2007  Artem Kaznatcheev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
# Not a valid entry point, skip unless MEDIAWIKI is defined
if (!defined('MEDIAWIKI')) {exit( 1 );}
 
$wgExtensionFunctions[] = "efRSSReader";
 
$wgExtensionCredits['parserhook'][] = array(
  'name' => 'RSS Reader 0.2.1',
  'author' => 'Artem Kaznatcheev',
  'description' => 'adds an rss tag',
  'url' => 'http://www.mediawiki.org/wiki/Extension:RSS_Reader'
);
 
### Global Variables ###
$egRSSReaderPath  = $wgScriptPath."/extensions/RSSReader"; //path to follow for server scripts
$egCacheTime      = 3600; //default cache time in seconds
$egCacheTimeMin   = 1800; //minimum cache time in seconds
$egCacheTimeMax   = 7200; //maximum cache time in seconds
$egCache          = true; //boolean to determine if caching should be done
 
/*
 *select if wikiRSS or lastRSS should be loaded
 *set rssType to the proper object type
 *each object of rssType must have the same interface
 */
if (file_exists(dirname( __FILE__ )."/wikiRSS.php" )) { 
  require_once ( dirname( __FILE__ )."/wikiRSS.php" ); //loads wikiRSS.php
  $rssType = wikiRSS; //set rssType to wikiRSS
} else if (file_exists(dirname( __FILE__ )."/lastRSS.php")) {
  require_once ( dirname( __FILE__ )."/lastRSS.php" ); //loads lastRSS.php
  $rssType = lastRSS; //set rssType to lastRSS
}
 
function efRSSReader() {
  global $wgParser;
  $wgParser->setHook( "rss", "efCreateRSSReader" );
}

/*JH 04.10.07 Helper function for sorting an associative Array by one member*/
function compare_cat($a, $b) {
	if (isset($a["category"]) && isset($a["category"]))
        return strnatcasecmp($a["category"], $b["category"]);
	else return false;
}
 
function efCreateRSSReader($input, $argv, &$parser){
  global $wgOut, $egRSSReaderPath, $egCacheTime, $egCacheTimeMin, 
    $egCacheTimeMax, $egCache, $rssType;
 
  $parser->disableCache(); //disable cache so feed is fetched everytime page is visited
  if (!$input){ //if no input do nothing
  } else {
    $fields = explode("|",$input); //get the urls between the tags
 
    /*
     * Check if a "number=n" argument has been provided
     * if it has and is an int, then set the $number field to the proper value
     * else set $number field to zero (which means "all available")
     */
    if (!$argv["number"]){ //check if argument has been provided
      $number=0; //set default if no argument has been provided
    } else {
      if ((int)$argv["number"]."" == $argv["number"]) { //check if argument is an integer 
        $number = $argv["number"]; //set $number field
      } else {
        $number=0; //if not an integer, then set default
      }
    }
 
    /*
     * Check if a "time=n" argument has been provided
     * if it has and is between $egCacheTimeMin and $egCacheTimeMax
     * then set $cacheTime to the value
     * else set $cacheTime to $egCacheTime (the default CacheTime)
     */
    if (!$argv["time"]){ //check if argument has been provided
      $cacheTime = $egCacheTime; //set default if no argument has been provided
    } else {
      if ((int)$argv["time"]."" == $argv["time"]) { //check if argument is an integer
        if (($argv["time"]>=$egCacheTimeMin)&&($argv["time"]<=$egCacheTimeMax)) { //check if argument is in range
          $cacheTime = $argv["time"]; //set $cacheTime field
        } else {
          $cacheTime = $egCacheTime; //set default if argument is outside range
        }
      } else {
        $cacheTime=$egCacheTime; //set default if not an integer
      }
    }
 
    /*
     * JH 04.10.07 erweitert um Option fuer author-Anzeige (Default: false)
     * Check if a "author=true" argument has been provided
     */
    if (!$argv["author"]){ //check if argument has been provided
      $author=false; //set default if no argument has been provided
    } else {
      if ($argv["author"] == "true") { 
        $author = true;
      } else {
	$author=false;
      }
    }

    /*
     * JH 04.10.07 erweitert um Option fuer description-Anzeige (Default: true)
     * Check if a "desc=true" argument has been provided
     */
    if (!$argv["desc"]){ //check if argument has been provided
      $desc=true; //set default if no argument has been provided
    } else {
      if ($argv["desc"] == "false") {
        $desc = false;
      } else {
        $desc=true;
      }
    }
    /*
     * JH 04.10.07 erweitert um Option fuer category-Anzeige (Default: false)
     * Check if a "cat=true" argument has been provided
     */
    if (!$argv["cat"]){ //check if argument has been provided
      $cat=false; //set default if no argument has been provided
    } else {
      if ($argv["cat"] == "true") {
        $cat = true;
      } else {
        $cat=false;
      }
    }

    $wgOut->addScript('<link rel="stylesheet" type="text/css" href="'.$egRSSReaderPath.'/RSSReader.css" />'); //add CSS
 
    $output = '
      <table id="RSSMainBody">
      <tr>
    ';
 
    $width = intval(100/sizeof($fields) - 5); //calculates the desired width for each feed and makes sure it is an int
 
    // Create wikiRSS or lastRSS object
    $rss = new $rssType; //initialize an object of rssType
    // Set public variables
    if (($rssType == lastRSS)&&($egCache)) $rss->cache_dir = dirname( __FILE__ ).'/cache/'; //directory of cache
    $rss->cache = $egCache; //cache attribute
    $rss->cache_time = $cacheTime; //refresh time in seconds 
    $rss->date_format = 'l';
 
    foreach ($fields as $field) {
      $output .= '<td valign="top" style="width: '.$width.'%;">'; //table cell that contains a single RSS feed
      if ($rssArray = $rss->get($field)){
        $output .=
          '<div class="RSSReader-head">'.
            '<h3><a href="'.
            $rssArray['link'].
            '">'.
            $rssArray['title'].
            '</a></h3>'.
            $rssArray['description'].
          '</div>';
 
        /*Outputs the items*/
	/*JH 04.10.07 erweiterte Ausgabe (category, author, description)*/

	//JH sort items by category, if asked for
	if ($cat){ 
		usort($rssArray['items'], "compare_cat");
	}

        $output .= '<ul>';
        $i = 0; //counter for number of items already displayed
		$curr_cat = ""; //JH for tree-like display of categories
		$cat_cnt = 0;
        foreach ($rssArray['items'] as $item){
			if ($cat && isset($item['category']) && $item['category']!=$curr_cat){
				if ($cat_cnt>0) $output.= '</ul>';
				$output.= '<li><b>'.$item['category'].'</b></li><ul>';
				$curr_cat = $item['category'];
				$cat_cnt ++;
			}

			if ($author && isset($item['author'])) $item_author = 'author/uploader: '.$item['author']."<br />";
			else $item_author = "";
			if ($desc && isset($item['description'])) $item_desc = $item['description'];
			else $item_desc = "";
	
			$output .= '<li><b><a href="'.$item['link'].'">'.$item['title'].'</a></b><br />'.
				$item_author.$item_desc.'</li>';
			$i += 1;
			if ($i == $number) { break; } //if reached the number of desired display items stop working on displaying more items
          } //close foreach items
		if ($cat_cnt >0) $output .= '</ul>'; //JH close last category
        $output .= '</ul>';
      } else { $output .= "Error: It's not possible to get $field..."; } //output error if not possible to fetch RSS
      $output .= '</td>';
    } //close foreach fields
    $output .= "</tr></table>";
  } //close main "else"
  return $output;
}
 
?>
