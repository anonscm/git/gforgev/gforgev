<?php
$wgExtensionFunctions[] ="wfExtensionSpecialHelp";
function wfExtensionSpecialHelp()
{
    global $wgMessageCache;
    $wgMessageCache->addMessages(array('evolvis_help' => 'Evolvis Help page'));
    require_once('includes/SpecialPage.php');
    SpecialPage::addPage(new SpecialPage('Evolvis_Help'));
}
?>         

