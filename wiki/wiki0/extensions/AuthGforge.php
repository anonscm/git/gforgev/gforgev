<?php
 # Copyright (C) 2004 Brion Vibber <brion@pobox.com>
 # http://www.mediawiki.org/
 #
 # This program is free software; you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation; either version 2 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License along
 # with this program; if not, write to the Free Software Foundation, Inc.,
 # 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 # http://www.gnu.org/copyleft/gpl.html
 

require_once("includes/AuthPlugin.php");

class AuthGforge extends AuthPlugin {
	var $gforge_db = NULL;
	
	//Constructor (called in LocalSettings.php)
	function AuthGforge($host, $port, $dbname, $user, $password) {
		$conn_string = "host=".$host." port=".$port." dbname=".$dbname." user=".$user." password=" .$password;
		$this->gforge_db = pg_connect($conn_string) or die ("Could not connect to database --> " . pg_last_error($this->gforge_db));	
	}

	//utility function: getting the user data from Gforge
    //works only if $wgGforgeGroup is set correctly in LocalSettings.php
	function getGforgeUser($username) {
		//check if anybody messed with $wgGforgeGroup
		global $wgGforgeGroup;
        $check = "/^[a-z0-9-]*$/";
		if (!isset($wgGforgeGroup) || !preg_match($check,$wgGforgeGroup)) {
            $wgGforgeGroup = "FALSE"; //no SQL error, just get no result
        }
		$username = addslashes(strtolower($username));
		
		//get only status=active (not suspended, not deleted)
		$gforge_find_user_query = 
            "SELECT u.*, g.group_id FROM users AS u, user_group AS ug, groups AS g ".
            "WHERE u.user_name='" . $username . "' ".
            "AND u.status='A' ".
            "AND u.user_id=ug.user_id AND ug.group_id=g.group_id ".
            "AND g.unix_group_name='".$wgGforgeGroup."'";
		$gforge_find_result = pg_query($this->gforge_db,$gforge_find_user_query);
        // make sure that there is only one person with the username
		if (pg_num_rows($gforge_find_result) == 1) {
			$user_array = pg_fetch_assoc($gforge_find_result);
			return $user_array;	
		}
		else {
			return false;
		}
	}
    
    //checks in Gforge whether user with user_id $uid has 
    //role "Admin" in group with group_id $gid
    function isAdmin($uid, $gid){
        $gforge_admin_query = 
            "SELECT r.role_name FROM user_group AS ug, role as r ".
            "WHERE ug.user_id=". $uid ." AND ug.group_id=". $gid ." ".
            "AND ug.role_id=r.role_id AND r.role_name='Admin'";

		$gforge_admin_result = pg_query($this->gforge_db,$gforge_admin_query);
        // no rows returned - it is no Admin
		if (pg_num_rows($gforge_admin_result) == 0) {
            return false;
		}
		else {
			return true;
		}
    }
    
	/* -------------------- AuthPlugin Functions ------------------------- */
	
	//Authentication calls 
    // if user is not in MW schema:
    //   1) autoCreate, 2) userExists, 3) authenticate. Then: initUser
    // if user is in schema: authenticate. Then: updateUser
	
	function userExists( $username ) {
		if ($this->getGforgeUser($username)) {
			return true;
		}
		else return false;
	}

	function authenticate( $username, $password ) {
		$user_array = $this->getGforgeUser($username);
		if ($user_array && md5($password) == $user_array['user_pw']) {
			return true;
		}
		else {
			return false;
		}
	}

	function modifyUITemplate( &$template ) {
			# Override this!
			$template->set( 'usedomain', false );
	}

	function setDomain( $domain ) {
			$this->domain = $domain;
	}

	function validDomain( $domain ) {
			# Override this!
			return true;
	}

    //later login, user is already in wiki database (schema)
	function updateUser( &$user ) {
		$name = $user->getName();
		$gforge_user = $this->getGforgeUser($name);

		if ( empty( $gforge_user ) ) {
			return;
		}

		$user->load();
		
		//overwrite the user's settings in MW
		$user->setRealName( $gforge_user['realname'] );
		$user->setEmail($gforge_user['email'] );
        
        //if user is Gforge project admin, but not sysop, make her Wiki sysop
        if ($this->isAdmin($gforge_user['user_id'], $gforge_user['group_id']) && 
            !in_array("sysop", $user->getEffectiveGroups())) {
            $user->addGroup('sysop');
        }
        //if user is sysop, but not Gforge project admin (anymore), 
        //remove her sysop status
        if (!$this->isAdmin($gforge_user['user_id'], $gforge_user['group_id']) && 
            in_array("sysop", $user->getEffectiveGroups())) {
            $user->removeGroup('sysop');
        }      

		$user->saveSettings();
		return true;
	}

	//if there is no such user in MW, create one
	function autoCreate() {
			return true;
	}

	//can't change Gforge Password in MW
	function allowPasswordChange() {
			return false;
	}

	//can't set Password in MW
	function setPassword( $user, $password ) {
			return false;
	}

	function updateExternalDB( $user ) {
			return true;
	}

	function canCreateAccounts() {
			return false;
	}

	function addUser( $user, $password, $email='', $realname='' ) {
			return true;
	}

	//do not check accounts against local database
	function strict() {
			return true;
	}

    //first login, adds user to Wiki Database (schema)
	function initUser( $user, $autocreate=false ) {
		$name = $user->getName();
		$gforge_user = $this->getGforgeUser($name);

		if ( empty( $gforge_user ) ) {
			return;
		}

		$user->load();
		//get some stuff from the Gforge account (user can change this later for MW)
		$user->setRealName( $gforge_user['realname'] );
		$user->setEmail($gforge_user['email'] );
		$user->mEmailAuthenticated = wfTimestampNow();
        
        //if user is Gforge project admin, make her Wiki sysop
        if ($this->isAdmin($gforge_user['user_id'], $gforge_user['group_id'])){
            $user->addGroup('sysop');
        }
		$user->saveSettings();
	}

	function getCanonicalName( $username ) {
			return $username;
	}

}

?>
