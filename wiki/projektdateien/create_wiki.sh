#! /bin/bash
# (c) Jutta Horstmann (jh@dataintransit.com), 9.10.07
#
# run this on the wiki server
# this script expects the necessary users and permissions already
# set in the database server
# 
# Usage: Give name of Gforge project as parameter 
# (and optionally path to main wiki)
#
# Changes
# Author    Date        Comment
# JH        17.10.07    $wgNavigationLinks for link to Evolvis project page
# JH        18.10.07    Check if target directory exists already


if [ $# -ne 0 ]
then
    gfgroup=$1

    # Do only edit if you changed the system setup
    webroot=/var/www
    mainwiki=wiki0
    domain=http://your.domain
    
    # Edit if you want to:
    # writeable directories are chowned to www-data (apache user)
    # -> change to other user or use chmod a+w if that suits you better
    apacheuser=www-data
    apachegroup=www-data
    
    # PostgreSQL variables (the database, schema and user is already set before in PG)
    pg_host=DB_host
    pg_dbmw=mw_evolvis
    
    # same password for every wiki admin, change here or later in wiki
    mw_sysoppw=your_password
    # Password for schema user in PG, change here or later in PG & LocalSettings.php 
    # has to be the same as set in setup_wikidb (creates user in PG)
    pg_dbpw=your_password
    
    target=$webroot/$gfgroup
    # Does target directory exist already?
    if [ -d $target ]
    then         
        echo "Target directory $target exists." 
        echo "Exiting..."
        exit 0
    fi
    iscript=$target/config/index.php
    iwebscript=$domain/$gfgroup/config/index.php

    #Logging
    outputlog=/tmp/"$gfgroup"_output.log
    errorlog=/tmp/"$gfgroup"_error.log
    
    
    
    if [ $# -eq 2 ]
    then
        base=$2
    else
        base=$webroot/$mainwiki
    fi

    # create directories
    mkdir $target
    cd $target
    mkdir images
    mkdir config
    chown -R $apacheuser:$apachegroup $target
    echo "Directories created."
    # Link stuff from main wiki (with exceptions)
    ln -s $base/* .
    rm LocalSettings.php
    cd $target/config
    ln -s $base/config/index.php .
    echo "Files linked."

    # execute MediaWiki install script
    /usr/bin/php $iscript > $outputlog 2>$errorlog
    
    # parse output for "Environment checked. You can install MediaWiki."
    # return 1 if found once
    mw_ok=`grep -m1 -c "Environment checked. You can install MediaWiki." $outputlog`
    if [ $mw_ok -eq 0 ]
    then
        echo "MediaWiki install started with errors. Please check the logfiles." 
        echo "You may then proceed by deleting the project wiki's directory or by commenting out the directory creation steps in this script." 
        echo "Exiting..."
        exit 0
    else 
    # ask user if she wants to proceed after checking the logs
        echo "MediaWiki install triggered successfully. You may now check the logfiles."
        echo "Do you want to proceed with the installation? (y/n)?"
        read proceed
        if [ "$proceed" = n ]
        then
            echo "Exiting..."
            exit 0
        fi
    fi
    
    # uppercase project name for site name
    gfgroup_up=`echo $gfgroup | tr a-z A-Z`
    
    # set POST parameters
    sitename=Sitename\=$gfgroup_up
    sysopname=SysopName\=admin_$gfgroup
    sysoppw1=SysopPass\=$mw_sysoppw
    sysoppw2=SysopPass2\=$mw_sysoppw
    u2umail=Emailuser\=emailuser_disabled
    dbtype=DBtype\=postgres
    dbserver=DBserver\=$pg_host
    dbname=DBname\=$pg_dbmw
    dbuser=DBuser\=$gfgroup
    dbpw1=DBpassword\=$pg_dbpw
    dbpw2=DBpassword2\=$pg_dbpw
    dbschema=DBmwschema\=$gfgroup
    
    #TO DO Stuff is sent unencrypted (same when posting via web interface)
    poststring=$sitename\&$sysopname\&$sysoppw1\&$sysoppw2\&$u2umail\&$dbtype\&$dbserver\&$dbname\&$dbuser\&$dbpw1\&$dbpw2\&$dbschema
    
    # execute install script, passing POST parameters
    /usr/bin/wget -O $outputlog -a $errorlog --post-data "$poststring" $iwebscript
   
    # parse output for "Installation successful!", return 1 if found once
    mw_ok_step2=`grep -m1 -c "Installation successful!" $outputlog`
    if [ $mw_ok_step2 -eq 0 ]
    then
        echo "MediaWiki install stopped with errors. Please check the logfiles." 
        echo "You may then proceed by deleting the project wiki's directory and, if necessary, truncating the project's wiki schema - or by commenting out the successful steps in this script." 
        echo "Exiting..."
        exit 0
    else 
    # ask user if she wants to proceed after checking the logs
        echo "MediaWiki installed successfully. You may now check the logfiles."
        echo "Do you want to proceed with the configuration? (y/n)?"
        read proceed
        if [ "$proceed" = n ]
        then
            echo "Exiting..."
            exit 0
        fi
    fi

# configure LocalSettings.php
    mv $target/config/LocalSettings.php $target/
    (
    cat << ENDCONFIG

# Nutzer duerfen nur schreiben, wenn eingeloggt. Anonyme Nutzer duerfen keinen Account anlegen
\$wgGroupPermissions['*'    ]['createaccount']   = false;
\$wgGroupPermissions['*'    ]['read']            = true;
\$wgGroupPermissions['*'    ]['edit']            = false;
\$wgGroupPermissions['*'    ]['createpage']      = false;
\$wgGroupPermissions['*'    ]['createtalk']      = false;
# Disable all forms of MediaWiki caching
\$wgMainCacheType = CACHE_NONE;
\$wgMessageCacheType = CACHE_NONE;
\$wgParserCacheType = CACHE_NONE;
\$wgCachePages = false;
#Extensions
require_once( "\$IP/extensions/RSSReader/RSSReader.php");
require_once( "\$IP/extensions/SyntaxHighlight_GeSHi/SyntaxHighlight_GeSHi.php" );
define("MAGPIE_DIR", "/var/www/wiki0/extensions/magpierss/");
define("MAGPIE_CACHE_ON", true);
define("MAGPIE_CACHE_DIR", "/var/www/wiki0/extensions/magpierss/cache/");
define('MAGPIE_OUTPUT_ENCODING', "ISO-8859-1");
include(MAGPIE_DIR."rss_fetch.inc");

#Authentication per Gforge
\$wgShowIPinHeader = false;
require_once( "\$IP/extensions/AuthGforge.php" );
\$wgAuth = new AuthGforge(\$wgDBserver, \$wgDBport, 'gforge', \$wgDBuser, \$wgDBpassword);
#Zu welchem Gforge-Projekt gehoert dieses Wiki?
\$wgGforgeGroup = "$gfgroup";
#Navigation-Link zum Gforge-Projekt (Zeilenumbruch ist notwendig!)
\$wgNavigationLinks = '
** http://evolvis.org/projects/'.\$wgGforgeGroup.' | Evolvis project page';
ENDCONFIG
) >> $target/LocalSettings.php
    echo "The MediaWiki instance is all set up."
    echo "Point your browser to $domain/$gfgroup to enter the wiki."
    
else
        echo "Usage: create_mediawiki <gforge_project_name>"
        echo "   or: create_mediawiki <gforge_project_name> <main_wiki_path>"
fi
