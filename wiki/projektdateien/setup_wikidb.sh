#! /bin/bash
# (c) Jutta Horstmann (jh@dataintransit.com), 10.10.07
# Usage: Give name of Gforge project as parameter
# It will be used as name of the new schema in the MediaWiki
# database "mw_evolvis" and as the username of the owner of this 
# schema. This user will also get access to 3 tables in the GForge
# database (per role wikiusers)
#
# Note: If schema exists already, PostgreSQL will print out errors and do nothing

if [ $# -ne 0 ]
then
    gfgroup=$1
    
# run this as DB superuser
    if [ `whoami` != "postgres" ]
    then
        echo "Please run this script as user postgres."
        exit 0
    fi
    
    pg_dbmw=mw_evolvis
    pg_dbpw=your_password
    
    sqloutput=/tmp/sqloutput.log
# mw_evolvis
# give access to new wikiuser
# create schema for new wiki instance
/usr/bin/psql $pg_dbmw <<ENDSQL  > $sqloutput
    CREATE USER "$gfgroup" WITH PASSWORD '$pg_dbpw' INHERIT;
    GRANT wikiusers TO "$gfgroup";
    CREATE SCHEMA "$gfgroup" AUTHORIZATION "$gfgroup";
ENDSQL
    
    echo "Database environment for MediaWiki instance set up. Please check logfile $sqloutput for errors."

else
        echo "Usage: setup_wikidb <gforge_project_name>"
fi
