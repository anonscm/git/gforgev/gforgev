<?php
/**
 * GForge Help Facility
 *
 * Copyright © 2007 Tarent GmbH
 * Copyright 1999-2001 (c) VA Linux Systems
 * The rest Copyright 2002-2004 (c) GForge Team
 * http://gforge.org/
 *
 * @version   $Id$
 *
 * This file is part of GForge.
 *
 * GForge is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GForge; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

require_once('pre.php');

$HTML->header(array('title'=>$Language->getText('help','title',array($GLOBALS['sys_name']))));

print '<p>tarent GmbH, company for software development and it-consulting,</p>
<p>Heilsbachstr. 24, D-53123 Bonn</p>
<p>web: www.tarent.de; mail: info@tarent.de; fon: 0228 - 526750</p>
<p><a href="http://tarent.de/servlet/is/4373/">Tarent Impressum</a></p>
<p>
V.i.S.d.P.: Boris Esser, tarent GmbH
</p>
<p>
management: Elmar Geese; Tom Müller-Ackermann; Boris Esser
</p>
<p>
legal form: GmbH; magistrate’s court Bonn, HRB 51 68; USt-IdNr.: DE 122264941
</p>
<p>
legal notice for linked webpages:
</p>

<p>
The webpages that are linked to our webpage have been checked in all
conscience for legal validity: The TARENT GmbH does not adopt the
webpages’ content. The Internet (and by this the webpages themselves) is
not a statical medium. Therefor the contents of a linked page can change
later. It is neither technical nor humanely possible to check external
webpages constantly.
</p>
<p>
(See also the district court’s judgment from 12th, may 1998 in Hamburg -
312 O 85/98)
</p>';


$HTML->footer(array());

?>
