#! /bin/sh
# postinst script for gforge
#
# see: dh_installdeb(1)

# Support for new place for pg_hba.conf
# I only try to upgrade on the default cluster
if [ -x /usr/bin/pg_lsclusters ]
then 
	# We are with new postgresql working with clusters
	# This is probably not te most elegant way to deal with database
	# I install or upgrade on the default cluster if it is online
	# or I quit gently with a big message
	pg_version=`/usr/bin/pg_lsclusters | grep 5432 | grep online | cut -d' ' -f1`
	if [ "x$pg_version" != "x" ] 
	then 
		export pg_hba_dir=/etc/postgresql/${pg_version}/main/
	else
		echo "No database found online on port 5432"
		echo "Couldn't initialize or upgrade gforge database."
		echo "Please see postgresql documentation"
		echo "and run dpkg-reconfigure -plow gforge-db-postgresql"
		echo "once the problem is solved"
		echo "exiting without error, but gforge db will not work"
		echo "right now"
		exit 0
	fi
else
    	export pg_hba_dir=/etc/postgresql
fi

set -e
#set -x				# Be verbose, be very verbose.

# summary of how this script can be called:
#        * <postinst> `configure' <most-recently-configured-version>
#        * <old-postinst> `abort-upgrade' <new version>
#        * <conflictor's-postinst> `abort-remove' `in-favour' <package>
#          <new-version>
#        * <deconfigured's-postinst> `abort-deconfigure' `in-favour'
#          <failed-install-package> <version> `removing'
#          <conflicting-package> <version>
# for details, see /usr/share/doc/packaging-manual/
#
# quoting from the policy:
#     Any necessary prompting should almost always be confined to the
#     post-installation script, and should be protected with a conditional
#     so that unnecessary prompting doesn't happen if a package's
#     installation fails and the `postinst' is called with `abort-upgrade',
#     `abort-remove' or `abort-deconfigure'.

. /usr/share/debconf/confmodule

#DSFHELPER:replace-files#
#DSFHELPER:handle-mainconffile#
#DSFHELPER:create-random-pw#
case "$1" in
    configure)
	add_onevar_mainconfile default_trove_cat 18

	if ! grep -q '^pam_db_user=' /etc/gforge/gforge.conf ; then
	    echo "pam_db_user=gforge_pam" >> /etc/gforge/gforge.conf
	fi
	if ! grep -q '^pam_db_pw=' /etc/gforge/gforge.conf ; then
	    pam_db_pw=$(gen_random_pw)
	    cat >> /etc/gforge/gforge.conf <<-EOF
pam_db_pw=$pam_db_pw
EOF
	fi
	
	/usr/sbin/gforge-config

        # Patch DB configuration files
	/usr/lib/gforge/bin/install-db.sh configure-files
	replace_file ${pg_hba_dir}/pg_hba.conf

        # Setup our DB
	/usr/lib/gforge/bin/install-db.sh configure
	[ -f /var/lib/gforge/sf_dump_for_gforge ] && /usr/lib/gforge/bin/install-db.sh restore /var/lib/gforge/sf_dump_for_gforge && mv /var/lib/gforge/sf_dump_for_gforge /var/lib/gforge/sf_dump_for_gforge.ok && /usr/lib/gforge/bin/install-db.sh configure
	[ -f /var/lib/gforge/sf_dump_for_gforge ] && mv /var/lib/gforge/sf_dump_for_gforge /var/lib/gforge/sf_dump_for_gforge.ko
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 0
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
