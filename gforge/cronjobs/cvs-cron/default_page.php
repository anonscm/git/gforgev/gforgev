<?php
	/* $Id$ */

	$projectpage = ereg_replace('^([^\.]*)\.(.*)$',
	    'https://\2/projects/\1/', $_SERVER['HTTP_HOST']);

	header("Status: 301 Moved Permanently", true, 301);
	header("Location: ${projectpage}", true);
	echo "Please go to ${projectpage} instead!";
	exit;
?>
