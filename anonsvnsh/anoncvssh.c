/* $Id$ */
/* $MirOS: src/libexec/anoncvssh/anoncvssh.c,v 1.8 2006/06/02 00:01:47 tg Exp $ */

/*-
 * Copyright (c) 2007
 *	Thorsten Glaser <t.glaser@aurisp.de>
 * Copyright (c) 2004, 2005, 2006
 *	Thorsten "mirabile" Glaser <tg@mirbsd.de>
 *
 * Licensee is hereby permitted to deal in this work without restric-
 * tion, including unlimited rights to use, publicly perform, modify,
 * merge, distribute, sell, give away or sublicence, provided all co-
 * pyright notices above, these terms and the disclaimer are retained
 * in all redistributions or reproduced in accompanying documentation
 * or other materials provided with binary redistributions.
 *
 * All advertising materials mentioning features or use of this soft-
 * ware must display the following acknowledgement:
 *	This product includes material provided by Thorsten Glaser.
 *
 * Licensor offers the work "AS IS" and WITHOUT WARRANTY of any kind,
 * express, or implied, to the maximum extent permitted by applicable
 * law, without malicious intent or gross negligence; in no event may
 * licensor, an author or contributor be held liable for any indirect
 * or other damage, or direct damage except proven a consequence of a
 * direct error of said person and intended use of this work, loss or
 * other issues arising in any way out of its use, even if advised of
 * the possibility of such damage or existence of a nontrivial bug.
 *-
 * user shell to be used for chrooted access (anonymous or personali-
 * sed, read-only or read-write) to cvs and possibly rsync.
 * This programme requires ANSI C.
 */

/*
 * Copyright (c) 2002 Todd C. Miller <Todd.Miller@courtesan.com>
 * Copyright (c) 1997 Bob Beck <beck@obtuse.com>
 * Copyright (c) 1996 Thorsten Lockert <tholo@sigmasoft.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>
#include <stdlib.h>
#if defined(__OpenBSD__) || defined(__NetBSD__) || defined(__FreeBSD__)
#include <paths.h>
#endif
#include <pwd.h>
#include <unistd.h>
#include <errno.h>

/*
 * You may need to change this path to ensure that RCS, CVS and diff
 * can be found
 */
#ifndef _PATH_DEFPATH
#define _PATH_DEFPATH	"/bin:/usr/bin:/usr/sbin"
#endif

/*
 * This should not normally have to be changed
 */
#ifndef _PATH_BSHELL
#define _PATH_BSHELL	"/bin/sh"
#endif

/*
 * This is our own programme name
 */
#ifndef ANONCVSSH_NAME
#define ANONCVSSH_NAME	"anoncvssh"
#endif


/****************************************************************/

static const char progID[] = "@(#) " HOSTNAME " anonsvnsh"
    "\n@(#) $Id$"
    "\n@(#) $MirOS: src/libexec/anoncvssh/anoncvssh.c,v 1.8 2006/06/02 00:01:47 tg Exp $";

#ifdef USE_SYSLOG
#include <string.h>
#include <syslog.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#define LOG_FACILITY	LOG_DAEMON
#define LOG_PRIO	LOG_INFO
#define DO_LOG(x, ...)	syslog(LOG_NOTICE, x, ## __VA_ARGS__)
#define DO_LOG0(x)	syslog(LOG_NOTICE, x)
#else /* def USE_SYSLOG */
#define DO_LOG(x, ...)	/* nothing */
#define DO_LOG0(x)	/* nothing */
#endif /* ! def USE_SYSLOG */

char env_path[] = "PATH=" _PATH_DEFPATH;
char env_shell[] = "SHELL=" _PATH_BSHELL;

char *env[] = {
	NULL /* placeholder for $HOME */,
	env_path,
	env_shell,
	NULL
};

int
main(void)
{
	struct passwd *pw;
	char *chrootdir;
	char *s;
	char *homedir;
	char *uname;

	pw = getpwuid(getuid());
	if (pw == NULL) {
		fprintf(stderr, "no user for uid %d\n", getuid());
		exit(1);
	}
	if (pw->pw_dir == NULL) {
		fprintf(stderr, "no home directory\n");
		exit(1);
	}

#ifdef USE_SYSLOG
	openlog(ANONCVSSH_NAME, LOG_PID | LOG_NDELAY, LOG_FACILITY);
#endif

	setuid(0);
	if ((chrootdir = strdup(CHROOT_PATH)) == NULL) {
		perror("strdup");
		exit(1);
	}
	if (strncmp(pw->pw_dir, chrootdir, strlen(chrootdir))) {
		DO_LOG0("user's home directory not inside chroot!\n");
		exit(1);
	}
	s = pw->pw_dir + strlen(chrootdir);
	if (*s != '/') {
		DO_LOG0("No leading slash in user's home directory!\n");
		exit(1);
	}
	if ((homedir = strdup(s)) == NULL) {
		perror("strdup");
		exit(1);
	}
	if ((uname = strdup(pw->pw_name ? : "(null)")) == NULL) {
		perror("strdup");
		exit(1);
	}

	if (asprintf(&env[0], "HOME=%s", homedir) < 0) {
		perror("asprintf");
		exit(1);
	}

	if (chroot(chrootdir) == -1) {
		perror("chroot");
		exit(1);
	}
	chdir(homedir);

#ifdef DEBUG
	DO_LOG("anonsvnsh: authenticated user %u (%s)\n",
	    (unsigned)pw->pw_uid, uname);
	DO_LOG("chrooted to '%s' with home directory '%s'\n",
	    chrootdir, homedir);
#endif

	setuid(pw->pw_uid);
	free(chrootdir);
	free(homedir);

	/*
	 * programme now "safe"
	 */

	/* just execute svn, no matter what the client said */
#ifdef DEBUG
	DO_LOG("spawning '%s' -t --tunnel-user '%s'\n", PATH_SVN, uname);
#endif
	execle(PATH_SVN, "svnserve", "-t", "--tunnel-user", uname, NULL, env);

	perror("execle: svnserve");
	DO_LOG0("chaining to svn failed!");
	fprintf(stderr, "unable to exec Subversion server!\n");
	exit(1);
}
