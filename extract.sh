#!/bin/mksh
# $Id$

cd $(dirname $0)
rm -rf extracted
mkdir extracted
for a in *.deb; do
	[[ $a = gforge_* ]] && continue		# meta package
	ar p $a data.tar.gz | tar -C extracted -xzf -
done
rm -f *.changes *.deb *.dsc *.tar.gz
